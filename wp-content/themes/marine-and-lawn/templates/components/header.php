<?php

use Backstage\SetDesign\NavMenu\NavMenuView;
use Backstage\View\Link;
use ChildTheme\Options\GlobalOptions;

?>

<header class="header-nav" data-gtm="Header">
    <div class="header-nav__container container">
        <?php if (has_nav_menu('primary_navigation')): ?>
            <div class="header-nav__menu">
                <?= NavMenuView::createList('primary_navigation'); ?>
            </div>
        <?php endif; ?>
        <div class="header-nav__brand">
            <a class="header-nav__brand-link" href="<?= home_url() ?>">
                <?php if ($header_image = GlobalOptions::brandImage()): ?>
                    <?= $header_image->css_class('header-nav__brand-image') ?>
                <?php endif; ?>
            </a>
        </div>
        <div class="header-nav__cta">
            <?php if ($contact_link = GlobalOptions::contactLink()): ?>
                <?= Link::createFromField($contact_link)->addClass('button'); ?>
            <?php endif; ?>
        </div>
    </div>
</header>
