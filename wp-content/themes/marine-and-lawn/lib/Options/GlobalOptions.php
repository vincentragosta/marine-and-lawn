<?php

namespace ChildTheme\Options;

use Backstage\Models\OptionsBase;
use WP_Image;

/**
 * Class GlobalOptions
 * @package ChildTheme\Options
 * @method static WP_Image brandImage()
 * @method static array contactLink
 */
class GlobalOptions extends OptionsBase
{
    protected $default_values = [
        'brand_image' => null,
        'contact_link' => []
    ];

    protected function getBrandImage()
    {
        $brand_image = $this->get('brand_image');
        return WP_Image::get_by_attachment_id($brand_image);
    }
}
