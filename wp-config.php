<?php

// ==============================
// Composer autoloader if present
// ==============================
if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if (file_exists(__DIR__ . '/local-config.php')) {
    include(__DIR__ . '/local-config.php');
}

// ===================================================
// Initialize Situation defaults
// ===================================================
if (class_exists('\Situation\WPConfig')) {
    new \Situation\WPConfig(__DIR__);
}

define('WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'marineandlawn.test');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
define('WP_HOME', 'http://marineandlawn.test/');
define('WP_SITEURL', 'http://marineandlawn.test/');

// ================================================
// You almost certainly do not want to change these
// ================================================
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY', 'Y,_N1B`$7 w`TK@+rNoCpMW>-cN|{N]|zIAPo/G(_kwtXXH7Z*}t8S1m+dNU8;S~');
define('SECURE_AUTH_KEY', '9QQ]{[-,wo,0z|NmTQ1wvph_a-4-v]AN>AfXPZPZ:@tR!e^8SjEri+6Qlqbd7Z?9');
define('LOGGED_IN_KEY', '-^TI+lT7H|%w! C>ZW(fr_;^*DUx+Ov+Uy2BDR*:U0WBBI2To^?m[/^}e}VQg_gY');
define('NONCE_KEY', 'ygb$g}ITF;>G_!:DKHo|cW2wjgz7,wH#G?bH`258W-]zTom4_AUYR+mogFnV3>rM');
define('AUTH_SALT', '!3FeyFO|@:#C[TOC;J~HKmBQz;:!fg|zc6x{~W?Ckv~5LQ|hu]SR9Cqv%uCJi/bn');
define('SECURE_AUTH_SALT', 'hw)5|<N3$5D55&ZULPr&5aU+>j+|MIN*p4GKKk?z4OzY4g8|5xuDDhXlVCz`2^U^');
define('LOGGED_IN_SALT', ']p?xlnvtkcP74irlSmQ`M-lucdqgsSwshB[<{y]s5^Z(Y>=25wg6du{L[dOfs%Zl');
define('NONCE_SALT', '/<H|wt(48;K+x_`M)R|ta`Ki#KK:t-(A+ElA``xFK}i41r*k8X%-+P|]HbB|y(?4');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'sit_';

// ================================
// Language
// Leave blank for American English
// ================================
define('WPLANG', '');

// ===================
// Bootstrap WordPress
// ===================
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wp/');
}
require_once(ABSPATH . 'wp-settings.php');